<?php
	$dbuser = "postgres";
	$dbpass	= "sevima";
	$dbname	= "test";
	$dbport = "5432";
	$dbhost	= "localhost";


	$conn = pg_connect("host=$dbhost port=$dbport dbname=$dbname user=$dbuser password=$dbpass");
	if(!$conn) 
		die ("Error : Connection failed !!!!");
	/*else
		echo "koneksi berhasil";*/

	if(isset($_POST['nama'])) {
		extract($_POST);

		if(empty($_POST['id']))
			$sql = "insert into users(nama, email) values('".$nama."', '".$email."')";
			
		else
			$sql = "update users set nama = '".pg_escape_string($nama)."', email = '".pg_escape_string($email)."' where id = " . pg_escape_string($id);
		
		$exe = pg_query($conn, $sql);

		if($exe)
			echo "sukses";
		else
			echo "gagal " . pg_result_error($exe);
	}

	if(isset($_GET['act'])) {
		$act = $_GET['act'];

		if($act == 'edit') {
			$sql = pg_query($conn, "select * from users where id = " . $_GET['id']);
			$data = pg_fetch_row($sql);
		}
		else if($act == 'hapus') {
			$sql = pg_query($conn, "delete from users where id = " . $_GET['id']);

			header('Location: index.php');
		}
	}
?>


<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="col-md-4">
			<form action="" method="POST">
				Nama : <input type="text" name="nama" value="<?= $data[1]; ?>" /><br />
				Email : <input type="text" name="email" value="<?= $data[2]; ?>" /><br />
				<input type="hidden" name="id" value="<?= $data[0]; ?>" /><br />
				<input type="submit" value="Simpan" />
			</form>
		</div>
		<div class="col-md-8">
			Data : <br />
			<table>
				<tr>
					<th>Nama</th>
					<th>Email</th>
					<th>Aksi</th>
				</tr>
				<?php
					$viewsql = "select * from users order by nama";
					$result = pg_query($conn, $viewsql);

					$data = pg_fetch_all($result);
					foreach ($data as $row) {
						extract($row);
						?>
						<tr>
							<td><?= $nama; ?></td>
							<td><?= $email; ?></td>
							<td>
								<a href="?act=edit&id=<?= $id; ?>">Edit</a>
								|
								<a onclick="getDelete('<?= $id; ?>')">Hapus</a>
							</td>
						</tr>
						<?php 
					}
				?>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	function getDelete(id) {
		if(confirm('yakin mau dihapus'))
			document.location.href="?act=hapus&id=" + id;
	}
</script>
